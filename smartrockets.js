"use strict";

//TODO shorter time should be more important

var game;

var smartrockets = smartrockets || {};

smartrockets.populationSize = 100;
smartrockets.generationLifetime = 300;

(function() {
  game = new Phaser.Game(800, 600, Phaser.AUTO, '', {
    preload: preload,
    create: create,
    update: update,
    render: render
  });

  function preload() {
    game.load.crossOrigin = 'anonymous';
    game.load.image('rocket', 'rocket1.png');
    game.load.image('target', 'target-128x128.png');
    game.load.audio('ding', 'ding.mp3');
  }

  function create() {
    smartrockets.ding = game.add.audio('ding');
    smartrockets.ding.allowMultiple = true;

    smartrockets.rocketGroup = game.add.group();
    smartrockets.targetGroup = game.add.group();

    game.physics.startSystem(Phaser.Physics.P2JS);
    game.physics.p2.setImpactEvents(true);

    smartrockets.rocketCollisionGroup = game.physics.p2.createCollisionGroup();
    smartrockets.targetCollisionGroup = game.physics.p2.createCollisionGroup();
    smartrockets.obstaclesCollisionGroup = game.physics.p2.createCollisionGroup();

    let target = smartrockets.targetGroup.create(200, 200, 'target');
    smartrockets.target = target;
    game.physics.p2.enable(target, false);
    target.anchor.set(0.5);
    target.body.kinematic = true;
    target.body.setCircle(target.width / 2);
    target.body.setCollisionGroup(smartrockets.targetCollisionGroup);
    target.body.collides([smartrockets.rocketCollisionGroup]);

    smartrockets.population = new Population();

    game.input.onDown.add(function() {
      new Obstacle(game.input.mousePointer.x, game.input.mousePointer.y, 50, 50);
    });
  }

  function update() {
    game.time.advancedTiming = smartrockets.configPanel.showFPS;

    smartrockets.population.update();
  }

  function render() {
    if (smartrockets.configPanel.showFPS) {
      game.debug.text(game.time.fps + " FPS", 2, 14);
    }
  }

  var ConfigPanel = function() {
    this.mutationRate = 0.001;
    this.showFPS = false;
  };

  smartrockets.configPanel = new ConfigPanel();
  var gui = new dat.GUI();
  gui.add(smartrockets.configPanel, 'mutationRate', 0, 1);
  gui.add(smartrockets.configPanel, 'showFPS');



}());

