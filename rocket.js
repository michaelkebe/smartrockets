"use strict";

var smartrockets = smartrockets || {};

function Rocket(dna) {
  Phaser.Sprite.call(this, game, game.world.centerX, game.world.height - 30, 'rocket');

  if (dna) {
    this.dna = dna;
  } else {
    this.dna = new DNA();
  }

  this.shortestDistanceToTarget = 999999999;
  this.lifetime = 0;
  this.fitness = 0;
  this.tickCount = 0;

  game.add.existing(this);
  smartrockets.rocketGroup.add(this);

  game.physics.p2.enable(this, false);
  this.anchor.set(0.5);

  this.reachedTarget = false;

  this.body.setCollisionGroup(smartrockets.rocketCollisionGroup);

  this.body.collides([smartrockets.targetCollisionGroup], function(rocket) {
    smartrockets.ding.play();
    rocket.sprite.reachedTarget = true;
  });

  this.body.collides([smartrockets.obstaclesCollisionGroup]);
}

Rocket.prototype = Object.create(Phaser.Sprite.prototype);
Rocket.prototype.constructor = Rocket;

Rocket.prototype.update = function () {
  if (this.reachedTarget) {
    this.destroy();
    return;
  }

  this.tickCount++;

  if (this.dna.genes === undefined) {
    debugger;
  }

  this.body.force.x = this.dna.genes[this.lifetime].x;
  this.body.force.y = this.dna.genes[this.lifetime].y;
  this.lifetime++;

  this.angle = angleFromVelocity(this.body.velocity);
  this.body.angle = this.angle;

  function angleFromVelocity(vel) {
    var rad = Math.atan2(-vel.x, vel.y);
    return rad * (180 / Math.PI) + 90;
  }
  let distanceToTarget = Math.sqrt(Math.pow(smartrockets.target.x - this.x, 2) + Math.pow(smartrockets.target.y - this.y, 2));
  this.shortestDistanceToTarget =
    distanceToTarget < this.shortestDistanceToTarget ?
      distanceToTarget : this.shortestDistanceToTarget;

  this.evaluate();
};

Rocket.prototype.evaluate = function() {
  this.fitness = 1 / this.shortestDistanceToTarget;
  this.fitness = Math.pow(this.fitness, 2);

  if (this.reachedTarget) {
    this.fitness *= this.tickCount * this.tickCount * 10;
  }

};
