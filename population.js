"use strict";

function Population() {
  this.rockets = [];
  this.size = smartrockets.populationSize;

  this.dnaPool = [];

  this.tickCount = 0;

  for (let i = 0; i < this.size; i++) {
    this.rockets[i] = new Rocket();
  }
}

Population.prototype.next = function() {
  this.evaluate();
  this.selection();
};

Population.prototype.evaluate = function() {
  this.rockets.forEach(r => r.evaluate());
  let maxFitness = this.rockets.reduce((max, r) => r.fitness > max ? r.fitness : max, 0);

  this.dnaPool = [];
  this.rockets.forEach(r => {
    let normalizedFitness = r.fitness / maxFitness;
    var n = normalizedFitness * 100;
    for (let i = 0; i < n; i++) {
      this.dnaPool.push(r.dna);
    }
  });
};

Population.prototype.selection = function() {
  let newRockets = [];
  for (let i = 0; i < this.size; i++) {
    let dnaA = game.rnd.pick(this.dnaPool);
    let dnaB = game.rnd.pick(this.dnaPool);
    let childDNA = dnaA.crossover(dnaB);
    childDNA.mutate(smartrockets.configPanel.mutationRate);
    newRockets.push(new Rocket(childDNA));
  }

  this.rockets.forEach(r => r.destroy());

  this.rockets = newRockets;
};

Population.prototype.update = function() {
  this.tickCount++;
  if (this.tickCount == smartrockets.generationLifetime) {
    this.tickCount = 0;
    this.next();
  }
};