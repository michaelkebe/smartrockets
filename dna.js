"use strict";

function DNA(genes) {
  this.size = smartrockets.generationLifetime;

  if (genes) {
    this.genes = genes;
  } else {
    this.genes = randomGenes(this.size);
  }

  function randomGenes(numberOfGenes) {
    const FORCE_RANGE = 1000;
    let genes = [];
    for (let i = 0; i < numberOfGenes; i++) {
      genes[i] = {};
      genes[i].x = game.rnd.integerInRange(-FORCE_RANGE, FORCE_RANGE);
      genes[i].y = game.rnd.integerInRange(-FORCE_RANGE, FORCE_RANGE);
    }
    return genes;
  }
}

DNA.prototype.crossover = function (other) {
  let genes = new Array(this.size);
  for (let i = 0; i < this.size; i++) {
    genes[i] = game.rnd.integerInRange(0, 1) === 0 ? this.genes[i] : other.genes[i];
  }
  return new DNA(genes);
};

DNA.prototype.mutate = function (rate) {
  const FORCE_RANGE = 1000;
  for (let i = 0; i < this.genes.length; i++) {
    if (Math.random() <= rate) {
      this.genes[i] = {};
      this.genes[i].x = game.rnd.integerInRange(-FORCE_RANGE, FORCE_RANGE);
      this.genes[i].y = game.rnd.integerInRange(-FORCE_RANGE, FORCE_RANGE);
    }
  }
};