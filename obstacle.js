"use strict";

function Obstacle(x, y, w, h) {
  Phaser.Sprite.call(this, game, x, y, w, h, '');
  this.visible = false;
  game.physics.p2.enable(this, true);
  game.add.existing(this);
  this.body.setRectangle(w, h);

  this.body.kinematic = true;
  this.body.setCollisionGroup(smartrockets.obstaclesCollisionGroup);
  this.body.collides([smartrockets.rocketCollisionGroup]);
}

Obstacle.prototype = Object.create(Phaser.Sprite.prototype);
Obstacle.prototype.constructor = Obstacle;
